import 'dart:io';

void main(List<String> arguments) {
  print("input number : ");
  int a1 = int.parse(stdin.readLineSync()!);
  isPrime(a1);
}

void isPrime(int a1) {
  bool Prime = true;
  for (int i = 2; i <= a1 / 2; i++) {
    if (a1 % i == 0) {
      Prime = false;
      break;
    }
  }

  if (Prime) {
    print("$a1 is a Prime");
  } else {
    print("$a1 is a Not Prime");
  }
}
